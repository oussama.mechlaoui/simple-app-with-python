#!/usr/bin/python
# -*- coding: latin-1 -*-
import os
import menu


class Employe:

    emp_dic = {}
    menu_emp = '''0. Revenur au Menu principal
1. Ajouter un employé
2. Mettre à jour un employé
3. Supprimer un employé
4. Afficher un employé
Veuillez entrer votre choix'''
    menu_maj_emp = '''0. Terminer
1. Modifier CIN
2. Modifier Nom
3. Modifier Prenom
4. Modifier Date de Naissance
5. Modifier Adresse
Veuillez entrer votre choix'''

    def __init__(self):
        while True:
            print(self.menu_emp)
            num = input()
            if num.isdecimal():
                num = int(num)
                if num == 0:
                    menu.Menu()
                    break
                elif num == 1:
                    self.ajouter_employe()
                elif num == 2:
                    self.modifier_employe()
                elif num == 3:
                    print('Entrer le Cin de l\'à supprimer')
                    cin = input()
                    self.supprimer_employe(cin)
                elif num == 4:
                    self.afficher_employe()
                else:
                    print('Veuillez entrer un numéro valide')
            else:
                print('Veuillez entrer un numéro')

    def formulaire(self):
        os.chdir('.')
        liste_emp = []
        print('Entrer le CIN')
        cin = input()
        print('Saisir le Nom')
        nom = input()
        liste_emp.append(nom)
        print('Saisir le Prenom')
        prenom = input()
        liste_emp.append(prenom)
        print('Saisir la date de naissance')
        date_de_naissance = input()
        liste_emp.append(date_de_naissance)
        print('Saisir une Adresse')
        adresse = input()
        liste_emp.append(adresse)
        self.emp_dic.setdefault(cin, liste_emp)
        with open('employees.txt','a') as fich:
            fich.writelines(cin+ ': ' +nom+','+prenom+','+date_de_naissance+','+adresse+'\n')
        self.build_dic()

    def build_dic(self):
        os.chdir('.')
        with open('employees.txt','r') as fich:
            for line in fich.readlines():
                self.emp_dic.setdefault(line.split(':')[0],line[:-1].split(':')[1][1:].split(','))
        with open('employees.txt','w') as fich:
            for k,v in sorted(self.emp_dic.items(),key=lambda x:x[1][0]):
                fich.writelines(k+': '+','.join(v)+'\n')

    def remplir_dic(self):
        with open('employees.txt','r') as fich:
            for line in fich.readlines():
                self.emp_dic.setdefault(line.split(':')[0],line[:-1].split(':')[1][1:].split(','))

    def maj_dic(self):
        with open('employees.txt','w') as fich:
            for k,v in sorted(self.emp_dic.items(),key=lambda x:x[1][0]):
                fich.writelines(k+': '+','.join(v)+'\n')

    def ajouter_employe(self):
        self.formulaire()

    def modifier_employe(self):
        test = True
        while test:
            self.remplir_dic()
            print('Veuillez choisir le CIN de l\'employé')
            id = input()
            if id in self.emp_dic:
                while True:
                    print(self.menu_maj_emp)
                    choix = input()
                    global new_cin
                    if choix.isdecimal():
                        if int(choix) == 0:
                            test = False
                            break
                        elif int(choix) == 1:
                            print('Veuillez entrer le nouveau CIN')
                            new_cin = input()
                            self.emp_dic.update({new_cin : self.emp_dic.get(id)})
                            self.emp_dic.pop(id)
                            self.maj_dic()
                        elif int(choix) == 2:
                            print('Veuillez entrer le nouveau Nom')
                            new_nom = input()
                            if id in self.emp_dic:
                                self.emp_dic.get(id)[0] = new_nom
                            elif new_cin in self.emp_dic:
                                self.emp_dic.get(new_cin)[0] = new_nom
                            self.maj_dic()
                        elif int(choix) == 3:
                            print('Veuillez entrer le nouveau Prenom')
                            new_prenom = input()
                            if id in self.emp_dic:
                                self.emp_dic.get(id)[1] = new_prenom
                            elif new_cin in self.emp_dic:
                                self.emp_dic.get(new_cin)[1] = new_prenom
                            self.maj_dic()
                        elif int(choix) == 4:
                            print('Veuillez entrer la nouvelle date de naissance')
                            new_date = input()
                            if id in self.emp_dic:
                                self.emp_dic.get(id)[2] = new_date
                            elif new_cin in self.emp_dic:
                                self.emp_dic.get(new_cin)[2] = new_date
                            self.maj_dic()
                        elif int(choix) == 5:
                            print('Veuillez entrer la nouvelle date de naissance')
                            new_adresse = input()
                            if id in self.emp_dic:
                                self.emp_dic.get(id)[3] = new_adresse
                            elif new_cin in self.emp_dic:
                                self.emp_dic.get(new_cin)[3] = new_adresse
                                self.maj_dic()
            else:
                print('Employé inexistant')
                break

    def supprimer_employe(self,id):
        self.remplir_dic()
        if id in self.emp_dic.keys():
            self.emp_dic.pop(id)
        self.maj_dic()

    def afficher_employe(self):
        self.remplir_dic()
        for k,v in self.emp_dic.items():
            print(k+': '+','.join(v))
