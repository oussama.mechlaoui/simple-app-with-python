#!/usr/bin/python
# -*- coding: latin-1 -*-
import sys
from calendrier import *
from employe import *

class Menu:
    menu = '''0. Quitter
1. Gérer les employés
2. Gérer les congés
3. Gérer le calendrier
Veuillez choisir votre choix'''
    num = -1

    def __init__(self):
        self.list_menu()
        self.num = input()
        if self.num.isdecimal():
            self.selection(int(self.num))
        else:
            print('Veuillez entrer un numéro')

    def list_menu(self):
        print(self.menu)


    def employes(self):
        Employe()

    def conges(self):
        Employe()

    def calendar(self):
        Calendrier()

    def selection(self, num):
        if num == 0:
            sys.exit()
        elif num == 1:
            self.employes()
        elif num == 2:
            self.conges()
        elif num == 3:
            self.calendar()
        else:
            print('Veuillez entrer un numéro valide')

