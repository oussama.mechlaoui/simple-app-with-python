#!/usr/bin/python
# -*- coding: latin-1 -*-
import menu
import os


class Calendrier:
    os.chdir('.')
    feries = []
    fich = None
    menucal = '''0. Revenur au Menu principal
1. Ajouter un jour férié
2. Supprimer un jour férié
3  Afficher les jours fériés d'un mois
Veuillez entrer votre choix'''

    def __init__(self):
        self.remplir_liste()
        while True:
            print(self.menucal)
            num = input()
            if num.isdecimal():
                num = int(num)
                if num == 1:
                    print('Veuillez entrer votre jour')
                    jour = input()
                    self.ajouter(jour)
                    self.synchronize()
                elif num == 2:
                    print('Veuillez entrer votre jour à supprimer')
                    jour = input()
                    self.supprimer(jour)
                    self.synchronize()
                elif num == 3:
                    self.feries.clear()
                    self.afficher()
                elif num == 0:
                    self.feries.clear()
                    menu.Menu()
                    break
                else:
                    print('Veuillez insérer un numéro')
            else:
                print('Veuillez entrer un entrée valide')

    @staticmethod
    def remplir_liste(self):
        fich = open('feries.txt', 'r')
        for i in fich.readlines():
            self.feries.append(i[:-1])
        fich.close()


    def synchronize(self):
        fich = open('feries.txt',"w")
        for i in self.feries:
            fich.writelines(i+'\n')
        fich.close()


    def ajouter(self, jour):
        if jour in self.feries:
            print('Ce jour ferries existe')
        else:
            self.feries.append(jour)

    def supprimer(self, jour):
        if jour in self.feries:
            self.feries.remove(jour)
        else:
            print("Ce jour n'est pas un jour ferié")

    def afficher(self):
        self.remplir_liste()
        for i in self.feries:
            print(i)

